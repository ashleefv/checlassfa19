for tt=2:numberTimePoints
    cumulrel(tt) = 100*(3*dr*trapz((cdrugt0(1:NR) - cdrug(1:NR,tt))'.*radius(1:NR) .*radius(1:NR)));
end       