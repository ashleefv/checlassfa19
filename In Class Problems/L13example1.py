# -*- coding: utf-8 -*-
"""
Created on Mon Oct  7 08:39:18 2019

@author: Ashlee N. Ford Versypt
Demos for Lecture 13: Python parameter estimation

    Example 1
    $\frac{dx}{dt}=b_1-b_2 x$
"""
import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# Data for example 1
xaxisData = np.array( [0.5, 1.0, 5.0, 30.0]) # time
yaxisData = np.array( [0.5, 1.2, 2.5, 2.7]) # x

# Guesses for parameters b1 & b2 for example 1
b1guess = 0.25;
b2guess = 0.4;
parameterGuesses = np.array([b1guess,b2guess]) # the fit is sensitive to the values of parameterGuesses

# Need two functions for our model--one defining ODE and another to solve the ODE

# 1. Define ODE
def ODEex1_definition(x,t,args): #deriv(yvar,xvar,args)
    # args will use params = [b1, b2]
    b1 = args[0]
    b2 = args[1]
    dxdt = b1-b2*x
    return dxdt

# 2. Solve ODEs at xaxisData points and return calculated yaxisCalc values
#   using current values of the parameters
def ODEmodelex1(xaxisData,*params): # asterisk is important
    # initial condition for the ODE
    yaxis0 = 0
    yaxisCalc = np.zeros(xaxisData.size)
    for i in np.arange(0,len(xaxisData)):
        xaxisIncrement = 0.01 # adjust as needed to be smaller than the difference between two xaxisData points
        xaxisSpan = np.arange(0,xaxisData[i]+xaxisIncrement,xaxisIncrement)
        ycalcForWholeRange = odeint(ODEex1_definition,yaxis0,xaxisSpan,args=(params,)) # odeint(model,initial condition, xspan, args)
        yaxisCalc[i]=ycalcForWholeRange[-1] # calculated y at the end of the interval
        return yaxisCalc
    
# Estimate parameters for example 1
parametersoln, pcov = curve_fit(ODEmodelex1,xaxisData,yaxisData,p0=parameterGuesses)

plt.plot(xaxisData,yaxisData,'o')
yaxis0 = 0.0
xaxisIncrement = 0.01
xaxisForPlotting = np.arange(0,xaxisData[-1]+xaxisIncrement,xaxisIncrement)
yaxisCalcFromGuesses = odeint(ODEex1_definition,yaxis0,xaxisForPlotting,args=(parameterGuesses,))
yaxisCalculated = odeint(ODEex1_definition,yaxis0 ,xaxisForPlotting,args=(parametersoln,))
plt.plot(xaxisForPlotting, yaxisCalculated,'r-')
plt.plot(xaxisForPlotting,yaxisCalcFromGuesses,'g--') # before fitting
plt.xlabel('t')
plt.ylabel('x')
plt.show()
print(parametersoln)
