OPTIONS = optimoptions(@lsqcurvefit, 'Algorithm', 'trust-region-reflective', 'TolX', 1e-6, 'TolFun', 1e-6, 'StepTolerance', 1e-13, 'MaxFunEvals', 1000, 'MaxIter', 3000); % use default for first guess
[coefficients, resnorm_fitted] = lsqcurvefit(@(coefficients,Xdata) PKPD_model(coefficients, Xdata, sigma), coefficientsguess, Xdata, Ydata_weighted, LB, UB, OPTIONS)
