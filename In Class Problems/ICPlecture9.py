# -*- coding: utf-8 -*-
"""
Created on Sun Oct  1 14:27:35 2017

@author: Ashlee
"""

import numpy as np
import scipy.optimize
# from scipy.optimize import fsolve

def f(x):
    #x0,x1= x
    #x0 = x[0]
    out = np.zeros(2)
    out[0] = x[0]*np.cos(x[1])-4
    out[1] = x[0]*x[1]-x[1]-5
    return out
#   return out[0], out[1]
print('scipy.optimize.fsolve')
soln = scipy.optimize.fsolve(f,[1,1])
print(soln)

#soln = fsolve(f,[1,1])
print('scipy.optimize.root')
soln = scipy.optimize.root(f,[1,1])

print(soln)
