function L6_quad_example
    for tt=2:numberTimePoints
    % functions and NR for the polynomial
    % order in polyfits
    numeratorY=(cdrugt0(1:NR)-cdrug(1:NR,tt))'...
        .*radius(1:NR).*radius(1:NR);
    % fit numerator integrand with 10th 
    % order polynominal
    numerator_coef=polyfit(radius,numeratorY,10);
    cumulrel_num(tt)=quad(@(r) CRIN(numerator_coef,r),0,1,TOL);
    end
end

function cumulrel_integrandnum = CRIN(numerator_coef,r)
    cumulrel_integrandnum = polyval(numerator_coef,r)
end