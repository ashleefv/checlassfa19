function ICPL3Prob2(varargin)
%   make a very simple plot of two functions with user input of x values to use
if nargin < 3
    xmin = 0; %default value
    xmax=3;
    Nx=200;
else
    xmin = varargin{1};
    xmax = varargin{2};
    Nx = varargin{3};
end

%% set independent variable
x=linspace(xmin,xmax,Nx);

%% calculate function values
f = 3*x.^2;
g = x;

%% plot results
plot(x,f)
hold on
plot(x,g)
hold off
xlabel('x')
ylabel('functions of x')
legend('f(x) = 3x^2','g(x) = x','Location','Best');
title('A simple plot')
grid on

end

