%% call the ODE solver
% initial condition for the ODE solver
conc_t0 = [AngI_conc_t0; AngII_conc_t0; Renin_conc_t0]; %% CE AngI Value
% ODE solver options
options = odeset('RelTol',1e-12,'AbsTOL',1e-6);
[t,conc] = ode45(@(t,conc) ODE(t,conc,params),time,conc_t0,options);

AngI_conc = conc(:,1).*Mw_AngI*1000/10^6; % pg/ml   %% CE AngI Value
AngII_conc = conc(:,2).*Mw_AngII*1000/10^6; % pg/ml
Renin_conc = conc(:,3).*Mw_Renin*1000/10^6; % pg/ml

%% Local function: ODE
% Define the differential equations for concentrations of non-drug species
function d_conc_dt = ODE(t,conc,params)  %% CE AngI Value
    
    % Input concentration vector conc contains species AngI, AngII & Renin
    AngI_conc = conc(1); % angiotension I concentration nmol/ml  %% CE AngI Value
    AngII_conc = conc(2); % angiotension II concentration nmol/ml
    Renin_conc = conc(3); % renin concentration nmol/ml
    %%%%%%%%%%%%
    % PD model
    %%%%%%%%%%%%
    % Rxn 1
    % Production rate of Ang I from angiotensinogen --> Ang I in presence
    % of Renin with baseline and variable contributions. Only Renin changes 
    % due to drug presence.
    variable_prod_AngI = k_cat_Renin*(Renin_conc-Renin_conc_t0);  %% CE AngI Value
    r1 = variable_prod_AngI+baseline_prod_AngI; %% CE AngI Value
    %%%%%%%%%%%%
    % Rxn 2
    % Baseline production of Renin + feedback from AngII to Renin 
    % production
    r2 = baseline_prod_Renin + k_feedback*(AngII_conc_t0-AngII_conc)
    %%%%%%%%%%%%
    % Rxn 3
    % Degradation of Renin
    r3 = k_degr_Renin*Renin_conc;
    %%%%%%%%%%%%
    % Rxn 4
    % Degradation of Ang I
    r4 = k_degr_AngI*AngI_conc;     %% CE AngI Value
    %%%%%%%%%%%%
    % Rxn 5
    % Rate of Ang I --> Ang II catalyzed by ACE with AngI_conc and I/KI 
    % changing due to drug presence
    r5 = VmaxoverKm*AngI_conc*(1-Inhibition);  %% CE AngI Value
    %%%%%%%%%%%%
    % Rxn 6
    % Consumption rate of Ang II --> with AngII_conc being the only term 
    % thatchanges due to drug presence
    r6 = k_cons_AngII*(AngII_conc - AngII_conc_t0) + baseline_cons_AngII;
    
    % ODEs for the three changing hormone/enzyme concentrations
    d_AngI_conc_dt = r1-r4-r5;  %% CE AngI Value
    d_AngII_conc_dt = r5-r6;
    d_Renin_conc_dt = r2-r3;


    % concentration derivative vector has entries for Ang I, Ang II, & Renin
    d_conc_dt(1) = d_AngI_conc_dt; %% CE AngI Value
    d_conc_dt(2) = d_AngII_conc_dt;
    d_conc_dt(3) = d_Renin_conc_dt;
    d_conc_dt = d_conc_dt';
end