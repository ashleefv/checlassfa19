function param_estimODE
tdata = [0 0.5 1 5 30];
xdata = [0 0.5 1.2 2.5 2.7];

x0 = 0;
b1guess = 1;
b2guess = 1;
parameterguesses = [b1guess, b2guess];

    function output = ODEmodel(parameters, t)

        [tsoln,xsoln] = ode45(@(t,x)deriv(t,x,parameters),t,x0);
        output = xsoln';
    end

    function dxdt = deriv(t,x,parameters)
        b1 = parameters(1);
        b2 = parameters(2);
        dxdt = b1-b2*x;
    end

[parametersoln,resnorm,residual,exitflag,output] = lsqcurvefit(@ODEmodel,parameterguesses,tdata,xdata)
plot(tdata,xdata,'o')
hold on
tplot = linspace(tdata(1),tdata(end),101);
plot(tplot,ODEmodel(parametersoln,tplot))
plot(tplot,ODEmodel(parameterguesses,tplot))
hold off
b1 = parametersoln(1);
b2 = parametersoln(2);
legend('data',['model with b1 =  ' num2str(b1) ' b2 = ' num2str(b2)],['model with guesses b1 =  ' num2str(b1guess) ' b2 = ' num2str(b2guess)])
end