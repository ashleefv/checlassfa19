%% Example 2
% $\frac{dx_1}{dt}=-b_1 x_1 x_2\quad\frac{dx_2}{dt}=b_1x_1 x_2-b_2 x_2$

%% Data for example 2
xdata = [0.5, 1, 5, 20]; %time
ydata = [99, 98, 50, 3; 2, 4, 35, 7]; %x1; x2
% alternative
% x1 =[99, 98, 50, 3];
% x2 = [2, 4, 35, 7];
% ydata = [x1; x2];

%% Guesses for parameters b1 & b2 for example 2
b(1) = 0.01;
b(2) = 0.2;
params_guess = b;

%% Initial conditions for ODEs in example 2
x0(1) = 100;
x0(2) = 1;

% alternative
%  x0 = [x1_0, x2_0];

%% Options for lsqcurvefit
OPTIONS = optimoptions(@lsqcurvefit,'TolX', 1e-14, ...
    'TolFun', 1e-14,'StepTolerance',1e-14); 

%% Estimate parameters for example 2
[params,resnorm,residuals,exitflag,] = lsqcurvefit(@(params,xdata) ...
    ODEmodel2(params,xdata,x0),params_guess,xdata,ydata, [], [], OPTIONS)
% try with x0 as the first entry in xdata,ydata

figure(2)
hold on
plot(xdata,ydata(1,:),'rx')
xlabel('t')
ylabel('x')
plot(xdata,ydata(2,:),'b+')
y_calc = ODEmodel2(params,xdata,x0);
plot(xdata,y_calc(1,:),'r-o')
plot(xdata,y_calc(2,:),'b-d')
xforplotting = linspace(xdata(1),xdata(end),101);

y_calcrefined = ODEmodel2(params,xforplotting,x0);
plot(xforplotting,y_calcrefined(1,:),'k-o')
plot(xforplotting,y_calcrefined(2,:),'g-d')
hold off
legend('x_1 data','x_2 data','x_1 fit','x_2 fit')

%% Define ODE for example 2
% Uses params = [b_1, b_2]
function dxdt = ODE2(t,x,params)
    b_1 = params(1);
    b_2 = params(2);
    dxdt(1) = -b_2*x(1)*x(2);
    dxdt(2) = b_1*x(1)*x(2)-b_2*x(2);
    dxdt = dxdt';
end

%% Solve ODE for example 2
% Uses current params values and xdata as the final point in the tspan for the ODE solver
function y_output = ODEmodel2(params,xdata,x0)
    for i = 1:length(xdata);
        tspan = [0:0.1:xdata(i)];
        [~,y_calc] = ode23s(@(t,x) ODE2(t,x,params),tspan,x0);
        y_output(i,:)=y_calc(end,:);
    end
    y_output = y_output';
end