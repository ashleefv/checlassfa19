# -*- coding: utf-8 -*-
"""
Created on Tue Oct  4 14:23:38 2016

@author: Ashlee N. Ford Versypt
Demos for Lecture 13: Python parameter estimation

    Example 1
    $\frac{dx}{dt}=b_1-b_2 x$
    
    Example 2
    $\frac{dx_1}{dt}=-b_1 x_1 x_2\quad\frac{dx_2}{dt}=b_1x_1 x_2-b_2 x_2$
"""
import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# Data for example 2
xaxisData = np.array( [0.5, 1, 5, 20] ) # time 
#UPDATE: for multiple rows, put each row in a [] and surround the whole thing by ([])
yaxisData = np.array( [ [99.0, 98.0, 50.0, 3.0], [2.0, 4.0, 35.0, 7.0] ] ) # x 

# Guesses for parameters b1 & b2 for example 2
#UPDATE: better initial guesses
b1guess = 0.01; 
b2guess = 0.2;   
parameterGuesses = np.array([b1guess, b2guess])

#Define ODEs for example 2
# Uses params = [b_1, b_2]
def ODE_definition(x,t,args): #deriv (yvar, xvar,arguments)
    x1 = x[0]
    x2 = x[1]
    b1 = args[0]
    b2 = args[1]
    dx1dt = -b1*x1*x2
    dx2dt = b1*x1*x1-b2*x2
    return dx1dt, dx2dt

# Define the model for example 2, which requires solution to a system of differential equations
# Uses current params values and xaxisData as the final point in the tspan for the ODE solver
def ODEmodel(xaxisData,*params):
    #Initial condition for ODE in example 2
    yaxis0 = np.array([100,1])
    numYaxisVariables = 2 #UPDATE
    yaxisOutput = np.zeros((xaxisData.size,numYaxisVariables)) # UPDATE
    for i in np.arange(0,len(xaxisData)):
        xaxisIncrement = 0.01 # adjust as needed for your problem
        xaxisSpan = np.arange(0,xaxisData[i]+xaxisIncrement,xaxisIncrement)
        y_calc = odeint(ODE_definition,yaxis0,xaxisSpan,args=(params,)) #final comma is weird but necessary
        yaxisOutput[i,:]=y_calc[-1,:]# UPDATE: this is now 2D with the number of columns set as : to include all yvariables
    # UPDATE: at this point we have a 2D matrix of yaxisOutput values. curve_fit 
    # needs a 1D vector that has the rows in a certain order, which result from the next two commands
    yaxisOutput = np.transpose(yaxisOutput)
    yaxisOutput = np.ravel(yaxisOutput)
    return yaxisOutput

#Estimate parameters for example 2
# UPDATE: np.ravel(yaxisData) transforms yaxisData from a 2D vector into the 1D vector that curve_fit expects.
parametersoln, pcov = curve_fit(ODEmodel,xaxisData,np.ravel(yaxisData),p0=parameterGuesses)

plt.plot(xaxisData, yaxisData[0,:],'o') 
plt.plot(xaxisData, yaxisData[1,:],'x') 
yaxis0 = np.array([100,1])
numYaxisVariables = 2
#UPDATE: use the ODEmodel to calculate y values that correspond to some input x values xforPlotting (don't have to be at xaxisData points)
xforPlotting = np.linspace(0,xaxisData[-1],100) # this creates 100 xaxis points between 0 and the final xaxisData point to be used for plotting a smooth function
y_calculated = ODEmodel(xforPlotting,*parametersoln)
#UPDATE: the answer from ODEmodel is 1D so we need to reshape it into the expected 2D matrix dimensions for plotting
y_calculated = np.reshape(y_calculated,(numYaxisVariables,xforPlotting.size))
plt.plot(xforPlotting, y_calculated[0,:],'b-',label='x1 fitted') #UPDATE
plt.plot(xforPlotting, y_calculated[1,:],'r-',label='x2 fitted') #UPDATE

conversion = 1 - y_calculated[0,:]
plt.xlabel('t')
plt.ylabel('x')
plt.show()
print('b1 =',parametersoln[0])
print(parametersoln)
print(pcov)