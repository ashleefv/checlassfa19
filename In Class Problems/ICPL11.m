% read in xdata,ydata
L11data

%define initial parameters guess
a0 = 1;
b0 = -1.6;
c0 = -0.75;
d0 = -2.7;

parameters0 = [a0,b0,c0,d0]; %guesses for where to start parameter estimation

%alternative defintion of the function
% fun = @(parameters,xdata) parameters(1)*exp(parameters(2)*xdata)+parameters(3)*exp(parameters(4)*xdata);
% call lsqcurvefit to estimate parameters
[parameters,resnorm,residual,exitflag,output] = lsqcurvefit(@fun,parameters0,xdata,ydata);

%plot the fitted solution
plot(xdata,fun(parameters,xdata),'gs')
hold on
plot(xdata,fun(parameters0,xdata),'rx')
%plot the data
plot(xdata,ydata,'o')
hold off
legend('model with fitted parameters','initial guess model','data')
figure(2)
plot(xdata,residual)
resnorm

% define the function to fit
function output = fun(parameters,t)
    %insert your function here
    a = parameters(1);
    b = parameters(2);
    c = parameters(3);
    d = parameters(4);
    y = a*exp(b*t)+c*exp(d*t);
    output = y;
end
