# checlassfa19

This repository stores codes for in class demonstration and example purposes.

## How do I get these files onto my computer? 
Open Git Bash on PC or terminal on MAC
```
$ cd locationonyourcomputerwhereyoustoreJUST_class_examples
```
This file location CANNOT also be the location of your homework folder and should be reserved for only the class examples.

### First time
```
$ git init
$ git remote add origin https://username@bitbucket.org/ashleefv/checlassfa19.git
$ git pull origin master
```
where username is your personal username for bitbucket. This should give you a copy of all of the files in this directory in your local folder locationonyourcomputerwhereyoustoreJUST_class_examples

You can repeat this process on lab computers or personal computers the very first time you want to connect to this online repository.

### After the first time
You only have readonly access to this repository, so you cannot add anything or push to it.
```
$ cd localdirectoryonyourcomputer
$ git pull origin master
```
That's all. You should do these two commands at the beginning of each class period. Both the problem statements and the solutions will be posted in this repository.